package pucrs.progoo;

public class Usuario {

	private String codigo;
	private String nome;
	private String cpf;
	
	public Usuario(String codigo, String nome, String cpf) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.cpf = cpf;
	}
	public String getCodigo() {
		return codigo;
	}
	public String getNome() {
		return nome;
	}
	public String getCpf() {
		return cpf;
	}
	

}
