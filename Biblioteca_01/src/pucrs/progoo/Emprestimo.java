package pucrs.progoo;

import java.time.LocalDate;

public class Emprestimo {

	private Usuario usuario;
	private Livro livro;
	private LocalDate dataLimite;
	private LocalDate dataDevolucao;
	private String estado;
	
	public Emprestimo(Usuario usuario, Livro livro, LocalDate dataLimite, LocalDate dataDevolucao, String estado) {
		super();
		this.usuario = usuario;
		this.livro = livro;
		this.dataLimite = dataLimite;
		this.dataDevolucao = dataDevolucao;
		this.estado = estado;
	}

	public LocalDate getDataDevolucao() {
		return dataDevolucao;
	}

	public void setDataDevolucao(LocalDate dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public Livro getLivro() {
		return livro;
	}

	public LocalDate getDataLimite() {
		return dataLimite;
	}
	
	public void finalizar(java.time.LocalDate dataDevolucao){
		
	}
}
